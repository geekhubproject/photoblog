var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var bodyParser = require('body-parser');
var expressSession = require('express-session');

var routes = require('./routes/index');
var home = require('./routes/home');
var login = require('./routes/login');
var signup = require('./routes/signup');
var users = require('./routes/users');
var upload = require('./routes/upload');
var comments = require('./routes/comments');
var images = require('./routes/images');
var download = require('./routes/download');
var products = require('./routes/products');
var personal = require('./routes/personal');
var album = require('./routes/album');
var admin = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressSession({
  secret: 'lego',
  resave: true,
  saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);
app.use('/home', home);
app.use('/users', users);
app.use('/login', login);
app.use('/signup', signup);
app.use('/upload', upload);
app.use('/images', images);
app.use('/download', download);
app.use('/comments', comments);
app.use('/admin', admin);
app.use('/*', function (req, res, next) {
 if (req.isAuthenticated()) next();
 else res.send(401);
});
app.use('/products', products);
app.use('/personal', personal);
app.use('/album', album);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
