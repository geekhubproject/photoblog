'use strict';

var gulp = require('gulp'),
    copy = require('gulp-copy');

gulp.task('copy-bower-css', function() {
  return gulp.src('./bower_components/**/*.min.css')
    .pipe(copy('./public/css/libs/', {prefix:4}))
});

gulp.task('copy-bower-js', function() {
  return gulp.src('./bower_components/**/*.min.js')
    .pipe(copy('./public/js/libs/', {prefix:4}))
});

gulp.task('copy-bower-fonts', function() {
  return gulp.src('./bower_components/bootstrap/fonts/*.*')
    .pipe(copy('./public/fonts/', {prefix:4}));
});

gulp.task('default', ['copy-bower-css', 'copy-bower-js', 'copy-bower-fonts']);