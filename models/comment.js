var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Comment = new Schema({
  username: { type: String, required: true },
  comments: { type: String, required: true }
});

module.exports = mongoose.model('Comment', Comment);