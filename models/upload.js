var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Upload = new Schema({
  idUser: { type: String, required: true },
  url: { type: String, required: true },
  date: { type: Date, default: Date.now },
  comments: [{ type: String }]
});

module.exports = mongoose.model('Upload', Upload);