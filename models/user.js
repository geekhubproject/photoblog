var mongoose = require('mongoose');
var crypto = require('crypto');

var SALT = 'gfdsa';

var Schema = mongoose.Schema;

var User = new Schema({
  id:  Schema.Types.ObjectId,
  username: {
    type: String,
    required: true,
    unique: true,
    dropDups: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    dropDups: true
  },
  password: {
    type: String,
    required: true,
    set: function (password) {
     return crypto.createHmac('sha1', SALT).update(password).digest('hex');
    }
  },
  date: {
    type: Date,
    default: Date.now
  },
  photo: [{type: String}],
  albums: [
    { name: {
      type: String
      }
    },
    {
      link: {
        type: String
      }
    },
    { date: {
      type:Date,
      default: Date.now
    }
    }
  ],
  comments: [
    {
      link: {
        type: String
      }
    },
    {
      date: {
        type: Date,
        default: Date.now
      }
    }
  ]
}
);

User.set('autoIndex', true);

module.exports = mongoose.model('User', User);