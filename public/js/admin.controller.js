angular
  .module('app')
  .controller('AdminController', AdminController);

function AdminController($scope, $http, $rootScope, $location) {
  $scope.list = [];

  $http.get('users/search/')
    .then(
      function (res) {
        $scope.list = res.data;
      },
      function (res) {
        $scope.failed = res.data;
      }
    );
}