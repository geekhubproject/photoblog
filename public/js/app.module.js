angular.module('app', ['ngRoute', 'angularFileUpload'])
  .config(['$routeProvider',
    function($routeProvider) {
      $routeProvider
      .when('/', {
        templateUrl: '/home'
      })
      .when('/login', {
        templateUrl: '/login',
        controller: 'AuthController'
      })
      .when('/signup', {
        templateUrl: '/signup',
        controller: 'SignUpController'
      })
      .when('/settings', {
        templateUrl: '/personal',
        controller: 'PersonalController'
      })
      .when('/album', {
        templateUrl: '/album',
        controller: 'AlbumController'
      })
      .when('/upload', {
        templateUrl: '/upload',
        controller: 'UploadController'
      })
      .when('/admin', {
        templateUrl: '/admin',
        controller: 'AdminController'
      })
      .otherwise({
        redirectTo: '/'
      });
    }]);