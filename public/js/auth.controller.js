angular
  .module('app')
  .controller('AuthController', AuthController);

  function AuthController($scope, $http, $rootScope, $location) {
    $scope.send = function () {
      var user = {
        username: $scope.email,
        password: $scope.password
      };

      $http.post('users/login', user)
        .then(
          function (res) {
              $rootScope.success = res.data.success ? res.data.data.user.username : res.data.data.details;
              $rootScope.username = res.data.data.user.username;
            if (res.data.success) {
              $location.path('/');
            }
          },
          function (res) {
            $scope.unsuccess = res.data;
          }
        );
    }
  }