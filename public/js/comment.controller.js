angular.module('app')
  .controller('CommentController', CommentController);

function CommentController($scope, $http) {
  $scope.data = [];
  $scope.successMessage = 'message';

  $scope.view = function () {

    $http({
      method: 'GET',
      url: '/comments'
    }).then(function successCallback(res) {
        $scope.data = res.data;
    }, function errorCallback(res) {

    });
  };

  $scope.send = function () {
    var userComment = {
      username: 'username-test',
      comments: $scope.comment
    };

    $http({
      method: 'POST',
      url: '/comments',
      data: userComment
    }).then(function successCallback(res) {
      $scope.successMessage = 'message send. Writer another!';
    }, function errorCallback(res) {
      $scope.error = res.data;
    });

    $scope.comment = '';
  }
}
