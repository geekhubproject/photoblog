angular.module('app')
  .controller('ImagesController', ImagesController);

function ImagesController($scope, $http) {
  $scope.data = [];
  
    $http({
      method: 'GET',
      url: 'images/'
    }).then(function successCallback(res) {
        $scope.data = res.data;
    }, function errorCallback(res) {

    });
  
}
