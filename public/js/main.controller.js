angular
    .module('app')
    .controller('MainController', MainController);

function MainController($scope) {
    $scope.plusOne = function() {
        $scope.picture.likes += 1;
    };
    $scope.plusOne1 = function() {
        $scope.picture1.likes += 1;
    };
    $scope.plusOne2 = function() {
        $scope.picture2.likes += 1;
    };
    $scope.plusOne3 = function() {
        $scope.picture3.likes += 1;
    };
    $scope.picture = { likes : 0 };
    $scope.picture1 = { likes : 0 };
    $scope.picture2 = { likes : 0 };
    $scope.picture3 = { likes : 0 };

}