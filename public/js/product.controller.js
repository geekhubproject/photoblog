angular
  .module('app')
  .controller('ProductController', [
    '$scope',
    'ProductFactory',
    function ($scope, Product) {
        refreshList();

        $scope.save = function () {
            Product.save($scope.product, function() {
                refreshList();
                $scope.product = {}
            });
        };

        function refreshList() {
            $scope.products = Product.query();
        }
    }
]);