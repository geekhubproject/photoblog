angular
  .module('app')
  .controller('SignUpController', SignUpController);

  function SignUpController($scope, $http) {
    $scope.send = function () {
      var user = {
        username: $scope.username,
        email: $scope.email,
        password: $scope.password
      };

      $http.post('users/signup', user)
        .then(
          function (res) {
            $scope.success = res.data.data.user.username + ' successfully registered!';
          },
          function (res) {
            $scope.unsuccess = 'Username or email exist';
          }
        );
    };
  }