var express = require('express');
var router = express.Router();
var Comment = require('../models/comment');

/* GET home page. */
router.get('/', function(req, res, next) {
  Comment.find({reportName: req.reportName}, function (err, comments) {
    if(err) {
      return next(err)
    }

    res.send(comments);
  });

});

router.post('/', function (req, res, next) {
  Comment.create(req.body, function (err, comment) {
    if(err) {
      return next(err)
    }
    res.send(comment);
  });
});
module.exports = router;
