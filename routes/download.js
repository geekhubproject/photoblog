var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  var required = req.url;

  if (required) {
    var filepath = req.url.split('?')[1];
    var img = fs.readFileSync('./images/' + filepath);

    res.writeHead(200, {'Content-Type': 'image/png' });
    res.end(img, 'binary');
  } else {
    res.writeHead(200, {'Content-Type': 'text/plain' });
    res.end('Image not found \n');
  }
});

module.exports = router;