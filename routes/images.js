var express = require('express');
var router = express.Router();
var Upload = require('../models/upload');
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
    Upload.find({reportName: req.reportName}, function (err, images) {
      if(err) {
        return next(err)
      }
      res.send(images);
    });
});

module.exports = router;