var express = require('express');
var router = express.Router();
var fs = require('fs');
var  multiparty = require('multiparty');
var Upload = require('../models/upload');
var User = require('../models/user');
var fileExtension = require('file-extension');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('upload', { title: 'Express' });
});

router.post('/', function (req, res, next) {

  var form = new multiparty.Form();
  var uploadFile = {uploadPath: '', type: '', size: 0};
  var maxSize = 2 * 1024 * 1024;
  var supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];
  var errors = [];

  form.on('error', function(err){
    if(fs.existsSync(uploadFile.path)) {
      fs.unlinkSync(uploadFile.path);
      console.log('error');
    }
  });

  form.on('close', function() {
    if(errors.length == 0) {
      res.send({status: 'ok', text: 'Success'});
    }
    else {
      if(fs.existsSync(uploadFile.path)) {
        fs.unlinkSync(uploadFile.path);
      }
      res.send({status: 'bad', errors: errors});
    }
  });

  form.on('part', function(part) {
    uploadFile.size = part.byteCount;
    uploadFile.type = part.headers['content-type'];
    var extension = fileExtension(part.filename);
    var filename = Date.now() + '.' + extension;
    uploadFile.path = './images/' + filename;

    if(uploadFile.size > maxSize) {
      errors.push('File size is ' + uploadFile.size / 1024 / 1024 + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
    }

    if(supportMimeTypes.indexOf(uploadFile.type) == -1) {
      errors.push('Unsupported mimetype ' + uploadFile.type);
    }

    if(errors.length == 0) {
      var out = fs.createWriteStream(uploadFile.path);
      part.pipe(out);

      console.log(req.user._id);

      var populate = {
        idUser: req.user._id,
        url: filename
      };

      Upload.create(populate, function (err, images) {
        if(err) {
          return next(err)
        }
      });

      User.findOne({ username: req.user._id }, function (err, doc) {
        if (err) return next(err);
        doc.photo = filename;
        doc.save();
      })
    }
    else {
      part.resume();
    }
  });

  form.parse(req);

});
module.exports = router;