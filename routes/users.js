var express = require('express'),
    passport = require('passport'),
    crypto = require('crypto'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('../models/user'),
    fs = require('fs-extra'),
    router = express.Router();

router.post('/signup', function(req, res, next) {
  User.create(req.body, function (err, user) {
    
    if(err) { return next(err); }

    req.login(user, function (err) {
      if (err) return next(err);
      res.json({
        success: true,
        data: {
          message: 'Sign succeeded',
          user: user
        }
      });
      res.redirect('/');
    });
  });
});

passport.use(new LocalStrategy(
  function (email, password, done) {
    var SALT = 'gfdsa';
    var hashPass = crypto.createHmac('sha1', SALT).update(password).digest('hex');
    
    User.findOne({
      email: email,
      password: hashPass
    }, '-password', function (err, user) {
      if (err) {
        return done(err)
      }
      if (!user) {
        return done(null, false, {message: 'Incorrect email or password.'});
      }
      return done(null, user);
      }
    )
  })
);

passport.serializeUser(function (user, done) {
  done(null, user && user._id);
});

passport.deserializeUser(function (id, done) {
  User.findOne({
    _id: id
  }, '-password', function (err, user) {
    if (err) {
      console.error(err);
      return done(err);
    }
    done(null, user);
  });
});

router.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) return next(err);
    if(!user) {
      return res.json({
        success: false,
        data: {
          message: 'Authentication failed',
          details: info.message
        }
      });
    }

    req.login(user, function (err) {
      if (err) return next(err);

      res.json({
        success: true,
        data: {
          message: 'Authentication succeeded',
          user: user
        }
      });
    });
  })(req, res, next);
});

router.get('/search', function (req, res, next) {
  User.find({reportName: req.reportName}, '-password', function (err, user) {
    if (err) return next(err);
    res.send(user);
  })
});

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

module.exports = router;